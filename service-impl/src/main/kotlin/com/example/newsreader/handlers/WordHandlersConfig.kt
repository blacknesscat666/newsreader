package com.example.newsreader.handlers

import com.example.newsreader.WordHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class WordHandlersConfig {

    @Autowired
    private val handlersList : Set<WordHandler> = HashSet()

    @Bean
    open fun handlers() : HandlerMap {
        return HandlerMap(handlersList.map { it.type() to it }.toMap())
    }
}