package com.example.newsreader.handlers

import com.example.newsreader.WordHandler

class HandlerMap(val values: Map<String, WordHandler>)