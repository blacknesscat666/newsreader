package com.example.newsreader.handlers

import com.example.newsreader.WordHandler
import org.springframework.stereotype.Component

@Component
class EntityHandler : WordHandler {
    override fun format(value: String): String {
        return "<strong>$value</strong>"
    }

    override fun type(): String = "Entity"
}