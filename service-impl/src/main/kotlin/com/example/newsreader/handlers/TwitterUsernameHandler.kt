package com.example.newsreader.handlers

import com.example.newsreader.WordHandler
import org.springframework.stereotype.Component

@Component
class TwitterUsernameHandler : WordHandler {
    override fun format(value: String): String {
        return "<a href=\"http://twitter.com/$value\">$value</a>"
    }

    override fun type(): String = "Twitter username"
}