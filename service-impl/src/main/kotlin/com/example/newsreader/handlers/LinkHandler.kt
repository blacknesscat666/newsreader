package com.example.newsreader.handlers

import com.example.newsreader.WordHandler
import org.springframework.stereotype.Component

@Component
class LinkHandler : WordHandler {
    override fun format(value: String): String {
        return "<a href=\"$value\">$value</a>"
    }

    override fun type(): String = "Link"
}