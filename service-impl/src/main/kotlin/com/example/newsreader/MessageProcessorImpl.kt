package com.example.newsreader

import com.example.newsreader.dto.ProcessedPostRes
import com.example.newsreader.dto.ValidPostReq
import com.example.newsreader.handlers.HandlerMap
import org.springframework.stereotype.Service

/**
 * Provides implementation for formatting source message with formatter's templates
 * @author Alexey Volshin
 * @version 1.0
 *
 * @param formatter - property map, contains templates for formatting
 */
@Service
class MessageProcessorImpl(private val handlers: HandlerMap) : MessageProcessor {

    override fun process(rawPost: ValidPostReq): ProcessedPostRes {
        var message = rawPost.message
        if (handlers.values.isEmpty()) {
            return ProcessedPostRes(message)
        }
        for (m in rawPost.metadata.sortedByDescending { it.from }) {
            val handler = handlers.values[m.type]
            if (handler != null) {
                message = message.replaceRange(m.from, m.to, handler.format(message.substring(m.from, m.to)))
            }
        }
        return ProcessedPostRes(message)
    }
}