package com.example.newsreader

import com.example.newsreader.dto.PostReq
import com.example.newsreader.dto.ValidPostMetadataReq
import com.example.newsreader.dto.ValidPostReq
import com.example.newsreader.exceptions.*
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import org.springframework.stereotype.Component

private const val NULL_SOURCE_MESSAGE = "source message must not be null"
private const val EMPTY_SOURCE_MESSAGE = "source message must not be empty"
private const val NULL_METADATA = "metadata must not be null"
private const val EMPTY_METADATA = "metadata must not be empty"
private const val NULL_META_TYPE = "type must not be null"
private const val EMPTY_META_TYPE = "type must not be empty"
private const val NULL_FROM_POSITION = "from position must not be null"
private const val NULL_TO_POSITION = "to position must not be null"
private const val INCORRECT_POSITION = "incorrect positions"
private const val CROSS_POSITION = "positions must not cross"
private const val POSITION_OUT_OF_BOUNDS = "position out of bounds"
private const val XSS_ATTACK_DETECTED = "xss attack detected"

/**
 * Provides implementation for validating source message and metadata
 * @author Alexey Volshin
 * @version 1.0
 */
@Component
class MessageValidatorImpl : MessageValidator {

    override fun validate(postReq: PostReq): ValidPostReq {
        val message = postReq.message ?: throw NewsReaderException(NULL_SOURCE_MESSAGE)
        if (message.isEmpty()) {
            throw NewsReaderException(EMPTY_SOURCE_MESSAGE)
        }
        if (!Jsoup.isValid(message, Whitelist.basic())) {
            throw NewsReaderException(XSS_ATTACK_DETECTED)
        }
        val metadata = postReq.metadata ?: throw NewsReaderException(NULL_METADATA)
        if (metadata.isEmpty()) {
            throw NewsReaderException(EMPTY_METADATA)
        }
        val validMetadata = mutableListOf<ValidPostMetadataReq>()
        for ((index, value) in metadata.sortedBy { it.from }.withIndex()) {
            val type = value.type ?: throw NewsReaderException(NULL_META_TYPE)
            if (type.isEmpty()) {
                throw NewsReaderException(EMPTY_META_TYPE)
            }
            val from = value.from ?: throw NewsReaderException(NULL_FROM_POSITION)
            val to = value.to ?: throw NewsReaderException(NULL_TO_POSITION)
            if (from >= to) {
                throw NewsReaderException(INCORRECT_POSITION)
            }
            if (from < 0 || to > message.length) {
                throw NewsReaderException(POSITION_OUT_OF_BOUNDS)
            }
            if (index < metadata.size - 1) {
                val nextValue = metadata[index + 1]
                val nextFrom = nextValue.from ?: throw NewsReaderException(NULL_FROM_POSITION)
                if (to > nextFrom ) {
                    throw NewsReaderException(CROSS_POSITION)
                }
            }
            validMetadata.add(ValidPostMetadataReq(type, from, to))
        }
        return ValidPostReq(message, validMetadata)
    }
}