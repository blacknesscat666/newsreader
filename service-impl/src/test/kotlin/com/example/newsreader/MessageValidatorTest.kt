package com.example.newsreader

import com.example.newsreader.dto.PostMetadataReq
import com.example.newsreader.dto.PostReq
import com.example.newsreader.exceptions.*
import net.bytebuddy.utility.RandomString
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class MessageValidatorTest {

    @Test
    fun `must fail as null message`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(null, null))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as empty message`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq("", null))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as null metadata`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10), null))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as empty metadata`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10), emptyList()))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as null meta type`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10), listOf(PostMetadataReq(null, null, null))))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as empty meta type`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10), listOf(PostMetadataReq("", null, null))))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as null from position`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10),
                    listOf(PostMetadataReq(RandomString.make(5), null, null))))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as null to position`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10),
                    listOf(PostMetadataReq(RandomString.make(5), 0, null))))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as position is incorrect`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10),
                    listOf(PostMetadataReq(RandomString.make(5), 10, 0))))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as position out of bounds`() {
        val validator = MessageValidatorImpl()
        val ex0 = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10),
                    listOf(PostMetadataReq(RandomString.make(5), -1, 20))))
        }
        Assertions.assertNotNull(ex0)
        val ex1 = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10),
                    listOf(PostMetadataReq(RandomString.make(5), 0, 11))))
        }
        Assertions.assertNotNull(ex1)
    }

    @Test
    fun `must fail as position is crossing`() {
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(RandomString.make(10),
                    listOf(PostMetadataReq(RandomString.make(5), 0, 5),
                            PostMetadataReq(RandomString.make(5), 3, 7))))
        }
        Assertions.assertNotNull(ex)
    }

    @Test
    fun `must fail as xss attack`() {
        val message = "Donald Trump post <a onClick=\"alert('u hacked')'>to</a> Twitter again. http://twitter.com"
        val validator = MessageValidatorImpl()
        val ex = assertThrows<NewsReaderException> {
            validator.validate(PostReq(message, listOf(PostMetadataReq(RandomString.make(5), 10, 0))))
        }
        Assertions.assertNotNull(ex)
    }
}