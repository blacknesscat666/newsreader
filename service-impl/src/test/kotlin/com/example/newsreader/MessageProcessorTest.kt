package com.example.newsreader

import com.example.newsreader.dto.ValidPostMetadataReq
import com.example.newsreader.dto.ValidPostReq
import com.example.newsreader.handlers.EntityHandler
import com.example.newsreader.handlers.HandlerMap
import com.example.newsreader.handlers.LinkHandler
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class MessageProcessorTest {

    @Test
    fun `must return formatted text`() {
        val message = "Donald Trump post to Twitter again. http://twitter.com"
        val outputMessage = "<strong>Donald Trump</strong> post to <strong>Twitter</strong> again. " +
                "<a href=\"http://twitter.com\">http://twitter.com</a>"
        val handlers = HandlerMap(mapOf("Entity" to EntityHandler(), "Link" to LinkHandler()))
        val messageProcessor = MessageProcessorImpl(handlers)
        val result = messageProcessor.process(ValidPostReq(message, listOf(
                ValidPostMetadataReq("Entity", 0, 12),
                ValidPostMetadataReq("Entity", 21, 28),
                ValidPostMetadataReq("Link", 36, 54))))
        Assertions.assertEquals(outputMessage, result.message)
    }
}