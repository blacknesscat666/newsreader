package com.example.newsreader

import com.example.newsreader.dto.PostMetadataReq
import com.example.newsreader.dto.PostReq
import com.example.newsreader.dto.ProcessedPostRes
import net.bytebuddy.utility.RandomString
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApiControllerTest(@Autowired val restTemplate: TestRestTemplate) {

    @Test
    fun `must return correct message`() {
        val message = "Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile"
        val outputMessage = "<strong>Obama</strong> visited <strong>Facebook</strong> headquarters: " +
                "<a href=\"http://bit.ly/xyz\">http://bit.ly/xyz</a> " +
                "@<a href=\"http://twitter.com/elversatile\">elversatile</a>"
        val body = PostReq(message, listOf(PostMetadataReq("Entity", 0, 5),
                PostMetadataReq("Entity", 14, 22),
                PostMetadataReq("Link", 37, 54),
                PostMetadataReq("Twitter username", 56, 67)))
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity = HttpEntity<PostReq>(body, headers)
        val response = restTemplate.postForEntity<ProcessedPostRes>("/process", entity)
        Assertions.assertEquals(HttpStatus.OK, response.statusCode)
        Assertions.assertEquals(outputMessage, response.body?.message)
    }

    @Test
    fun `must fails as incorrect input data`() {
        val message = RandomString.make(80)
        val body = PostReq(message, listOf(PostMetadataReq("Entity", 0, 5),
                PostMetadataReq("Entity", 14, 22),
                PostMetadataReq("Link", 37, 54),
                PostMetadataReq("Twitter username", 56, message.length + 1)))
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity = HttpEntity<PostReq>(body, headers)
        val response = restTemplate.postForEntity<ProcessedPostRes>("/process", entity)
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }
}