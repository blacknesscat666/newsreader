package com.example.newsreader.controller

import com.example.newsreader.MessageProcessor
import com.example.newsreader.MessageValidator
import com.example.newsreader.dto.PostReq
import com.example.newsreader.dto.ProcessedPostRes
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ApiController(private val messageProcessor: MessageProcessor,
                    private val messageValidator: MessageValidator) {

    @PostMapping("/process")
    fun process(@RequestBody message: PostReq) : ProcessedPostRes {
        return messageProcessor.process(messageValidator.validate(message))
    }
}