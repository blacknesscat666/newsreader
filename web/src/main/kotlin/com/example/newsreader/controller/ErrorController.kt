package com.example.newsreader.controller

import com.example.newsreader.exceptions.NewsReaderException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@RestControllerAdvice
class ErrorController {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleNewsReaderException(exception: NewsReaderException): Any {
        return mapOf("timestamp" to LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME),
                     "message" to exception.message)
    }
}