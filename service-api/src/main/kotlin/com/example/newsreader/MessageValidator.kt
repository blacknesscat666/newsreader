package com.example.newsreader

import com.example.newsreader.dto.PostReq
import com.example.newsreader.dto.ValidPostReq

/**
 * Validating message service
 * @author Alexey Volshin
 * @version 1.0
 */
interface MessageValidator {

    /**
     * Validates input message
     *
     * @param postReq - source text with list of metadata
     */
    fun validate(postReq: PostReq) : ValidPostReq
}