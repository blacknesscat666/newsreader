package com.example.newsreader.dto

data class ProcessedPostRes(val message: String)