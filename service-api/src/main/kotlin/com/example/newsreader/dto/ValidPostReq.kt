package com.example.newsreader.dto

data class ValidPostReq(val message: String, val metadata: List<ValidPostMetadataReq>)