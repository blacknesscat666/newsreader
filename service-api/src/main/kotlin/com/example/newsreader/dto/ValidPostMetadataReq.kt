package com.example.newsreader.dto

data class ValidPostMetadataReq(val type: String, val from: Int, val to: Int)