package com.example.newsreader.dto

data class PostMetadataReq(val type: String?, val from: Int?, val to: Int?)