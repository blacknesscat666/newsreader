package com.example.newsreader.dto

data class PostReq(val message: String?, val metadata: List<PostMetadataReq>?)