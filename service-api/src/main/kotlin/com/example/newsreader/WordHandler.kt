package com.example.newsreader

interface WordHandler {
    fun format(value: String): String
    fun type(): String
}