package com.example.newsreader

import com.example.newsreader.dto.ProcessedPostRes
import com.example.newsreader.dto.ValidPostReq

/**
 * Formatting message service
 * @author Alexey Volshin
 * @version 1.0
 */
interface MessageProcessor {

    /**
     * Process input message
     *
     * @param rawPost - source text with list of metadata
     */
    fun process(rawPost : ValidPostReq) : ProcessedPostRes
}