# Getting Started

### Build and Run

`./gradlew bootRun`

### Test with curl example

`curl -d '{"message":"Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile","metadata":[{"type":"Entity","from":0,"to":5},{"type":"Entity","from":14,"to":22},{"type":"Link","from":37,"to":54},{"type":"Twitter username","from":56,"to":67}]}' -H "Content-Type: application/json" -X POST http://localhost:8080/process`

#### Expected result

`{"message":"<strong>Obama</strong> visited <strong>Facebook</strong> headquarters: <a href=\"http://bit.ly/xyz\">http://bit.ly/xyz</a> @<a href=\"http://twitter.com/elversatile\">elversatile</a>"}`
