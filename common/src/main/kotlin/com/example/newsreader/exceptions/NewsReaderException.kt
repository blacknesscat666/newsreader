package com.example.newsreader.exceptions

class NewsReaderException(message: String) : Exception(message)